const express = require('express');
const path = require('path')
const port = 6942;
const app = express();
let indexPath = path.join(__dirname, "../dist/QueyrasApp/index.html");

app.use(express.static(indexPath))
app.get('*.js', (req, res) => {
  console.log(req.originalUrl)
  res.sendFile(path.join(__dirname, "../dist/QueyrasApp/" + req.originalUrl))
});
app.get('*.css', (req, res) => {
  console.log(req.originalUrl)
  res.sendFile(path.join(__dirname, "../dist/QueyrasApp/" + req.originalUrl))
});

app.get('*', (req, res) => {
  res.sendFile(indexPath)
});


app.listen(port, () => {
  console.log("Server is listening on port "+port);
});
