import { Component, OnInit } from '@angular/core';
import {ApiService} from '../services/api.service';
import {UserService} from '../services/user.service';
import { Pipe, PipeTransform } from '@angular/core';
import {environment} from '../../environments/environment';


@Component({
  selector: 'app-infos',
  templateUrl: './infos.component.html',
  styleUrls: ['./infos.component.css']
})
export class InfosComponent implements OnInit {
  user = JSON.parse(localStorage.getItem("user"))
  users_list = JSON.parse(localStorage.getItem("usersListComplete"))
  clicked = false
  participants_aller = []
  participants_aller_non = []
  participants_aller_short = []
  participants_aller_non_short = []
  places_aller = 0
  participants_retour = []
  participants_retour_non = []
  participants_retour_short = []
  participants_retour_non_short = []
  places_retour = 0
  nb_personnes_semaine_1 = 0
  places_semaine_1 = 0
  nb_personnes_semaine_2 = 0
  places_semaine_2 = 0
  lien_excel = environment.lienExcel
  lien_charte = environment.lienCharte
  display_details = 'add'
  lits_users = []
  lits = []
  jours = []
  editChambre = false
  afficherChambres = false
  afficherAutres = false
  afficherAdmin = false
  afficherProgramme = false;
  afficherMenus = false
  afficherMateriel = false
  programmes = [];
  editProgramme = false;
  programmes_suggestions = [];
  coeff_voiture = environment.coeff_voiture
  nb_semaine_1 = 0
  nb_semaine_2 = 0


  constructor(private apiService: ApiService, private userService: UserService) { }

  ngOnInit(): void {
    this.updateUser()
    this.refreshLits()
    this.refreshProgramme()
    this.refreshProgrammeSuggestions()
    this.apiService.getLits().subscribe((lits)=>{
      this.lits = lits
    });
    this.apiService.getJours().subscribe((jours)=>{
      this.jours = jours
    });
  }

  getUserFromLits(id_jour, id_lit): any{
    var user = this.lits_users.filter(x => (x.id_jour === id_jour && x.id_lit === id_lit))
    if(user.length == 0){
      return [{nom: '', id_user: ''}]
    }
    else{
      // console.log(user)
      return user
    }
  }

  getProgrammeFromJour(id) {
    var programme = this.programmes.filter(x => (x.jour === id))
    if(programme.length == 0){
      return [{jour: '', libelle: ''}]
    }
    else{
      // console.log(user)
      return programme
    }
  }


  updateLitRecursive(datas){
    this.apiService.updateLit(datas).subscribe((lits)=>{
      if(datas.id_jour != 7 && datas.id_jour != 14){
        datas.id_jour++
        this.updateLitRecursive(datas)
      } else{
        this.refreshLits()
      }
    })


  }
  onEditLit(id_jour, id_lit, id_user_old, id_user_new) {
    if(id_jour == 1 || id_jour == 8){
      var datas = {
        id_jour: id_jour,
        id_lit: id_lit,
        id_user_old: id_user_old,
        id_user_new: id_user_new,
      }
      this.updateLitRecursive(datas)

    } else{
      var datas = {
        id_jour: id_jour,
        id_lit: id_lit,
        id_user_old: id_user_old,
        id_user_new: id_user_new,
      }
      this.apiService.updateLit(datas).subscribe((lits)=>{
        this.refreshLits()
      });
    }
  }


  updateUser(){
    let that = this
    that.userService.refreshUser((user) => {
      this.user = user
      that.apiService.getPaiements(that.user.id).subscribe((paiements)=>{
        that.user.paiements = paiements
      });
      that.apiService.getAllUsers().subscribe((users)=>{
        var i = 0;
        step(users)
        function step(users) {
          if (i < users.length) {
            that.apiService.getPaiements(users[i].id).subscribe((paiements)=>{
              users[i].paiements = paiements
              if(i == (users.length - 1) ){
                that.clicked = false
                that.users_list = users
                for (var j = 0; j < that.users_list.length; j++){
                  if(that.users_list[j].semaine == 0 || that.users_list[j].semaine == 1){
                    that.nb_semaine_1 += 1
                  }
                  if(that.users_list[j].semaine == 0 || that.users_list[j].semaine == 2){
                    that.nb_semaine_2 += 1
                  }
                }
                that.refreshTrajets()
              }
              else{
                i++;
                step(users);
              }

            });
          }
        }
      })
    })

  }

  refreshLits(){
    this.apiService.getLitsUsers().subscribe((lits_users)=>{
      this.lits_users = lits_users
    });
  }

  refreshProgramme(){
    this.apiService.getProgramme().subscribe((programmes)=>{
      this.programmes = programmes
      this.clicked = false
    });
  }

  refreshProgrammeSuggestions(){
    this.apiService.getProgrammeSuggestions().subscribe((programmes_suggestions)=>{
      this.programmes_suggestions = programmes_suggestions
      this.clicked = false
    });
  }

  refreshTrajets(){
    this.participants_aller = []
    this.participants_aller_non = []
    this.participants_aller_short = []
    this.participants_aller_non_short = []
    this.places_aller = 0
    this.participants_retour = []
    this.participants_retour_non = []
    this.participants_retour_short = []
    this.participants_retour_non_short = []
    this.places_retour = 0
    this.nb_personnes_semaine_1 = 0
    this.places_semaine_1 = 0
    this.nb_personnes_semaine_2 = 0
    this.places_semaine_2 = 0
    var that = this
    that.apiService.getLieux().subscribe((lieux)=>{
      for (const user of that.users_list) {
        //COVOIT ALLER
        if(that.user.arrivee_lieu != null && that.user.arrivee_date != null){
          if(that.getLieuDepart(lieux, user.arrivee_lieu) == that.getLieuDepart(lieux, that.user.arrivee_lieu)
            && user.arrivee_date == that.user.arrivee_date){
            if(user.participation_covoit_aller == 1){
              if(user.id != that.user.id){
                that.participants_aller_short.push(user)
              }
              that.participants_aller.push(user)
              if(user.conducteur == 1) {
                // that.conducteurs_aller.push(user)
                that.places_aller += user.nb_places_covoit
              }
            } else {
              if(user.id != that.user.id){
                that.participants_aller_non_short.push(user)
              }
              that.participants_aller_non.push(user)
            }
          }
        }
        //COVOIT RETOUR
        if(that.user.depart_lieu != null && that.user.depart_date != null){
          if(that.getLieuDepart(lieux, user.depart_lieu) == that.getLieuDepart(lieux, that.user.depart_lieu)
            && user.depart_date == that.user.depart_date){
            if(user.participation_covoit_retour == 1){
              if(user.id != that.user.id){
                that.participants_retour_short.push(user)
              }
              that.participants_retour.push(user)
              if(user.conducteur == 1){
                // that.conducteurs_aller.push(user)
                that.places_retour += user.nb_places_covoit
              }
            } else {
              if(user.id != that.user.id){
                that.participants_retour_non_short.push(user)
              }
              that.participants_aller_non.push(user)
            }
          }
        }

        //VOITURE SUR PLACE SEMAINE 1
        if(user.semaine == 1 || user.semaine == 0){
          that.nb_personnes_semaine_1 += 1
          if(user.conducteur == 1){
            // that.conducteurs_aller.push(user)
            that.places_semaine_1 += user.nb_places
          }
        }
        //VOITURE SUR PLACE SEMAINE 2
        if(user.semaine == 2 || user.semaine == 0){
          that.nb_personnes_semaine_2 += 1
          if(user.conducteur == 1){
            // that.conducteurs_aller.push(user)
            that.places_semaine_2 += user.nb_places
          }
        }
      }
    })

  }

  getLieuDepart(lieux, lieu){
    for (const lieuxElement of lieux) {
      if(lieuxElement.libelle == lieu){
        return lieuxElement.depart
      }
    }
  }

  paye(id: string, user_id: string) {
    if(this.clicked) return
    this.clicked = true
    this.apiService.updatePaiement(id, user_id, 1, 1).subscribe((users)=>{
      this.updateUser()
    })

  }

  demande(id: string, user_id: string) {
    if(this.clicked) return
    this.clicked = true
    this.apiService.updatePaiement(id, user_id, 1, 0).subscribe((users)=>{
      this.updateUser()
    })
  }



  demandeNewCustom(id: string, event) {
    if(this.clicked) return
    this.clicked = true
    var montant = event.target.previousElementSibling.value
    if(montant == 0){
      this.clicked = false
      return
    }
    this.apiService.addPaiement(id, montant).subscribe((users)=>{
      this.updateUser()
    })
  }

  delete(id: string, user_id: string) {
    if(this.clicked) return
    this.clicked = true
    this.apiService.deletePaiement(id, user_id).subscribe((users)=>{
      this.updateUser()
    })
  }

  refreshAllUsers() {
    if(this.clicked) return
    this.clicked = true
    this.apiService.updateAllUsers().subscribe((users)=>{
      this.updateUser()
      alert("done")
    })
  }

  sendEmails() {
    this.apiService.sendEmail().subscribe((users)=>{
      alert("done")
    })
  }

  newProgramme(id_jour, event) {
    if(this.clicked) return
    this.clicked = true
    var programme_libelle = event.target.previousElementSibling.value
    this.apiService.updateProgramme(id_jour, programme_libelle).subscribe((users)=>{
      this.refreshProgramme()
    })
  }

  newSuggestion(event) {
    if(this.clicked) return
    this.clicked = true
    var programme_libelle = event.target.previousElementSibling.value
    event.target.previousElementSibling.value = ""
    console.log(programme_libelle)
    this.apiService.insertProgrammeSuggestion(programme_libelle).subscribe((result)=>{
      if(result != "ok"){
        alert("Une erreur s'est produite.")
        this.clicked = false
        return
      } else{
        this.refreshProgrammeSuggestions()
      }
    })
  }

  deleteSuggestion(id) {
    if(this.clicked) return
    this.clicked = true
    this.apiService.deleteSuggestion(id).subscribe((result)=>{
      if(result != "ok"){
        alert("Une erreur s'est produite.")
        this.clicked = false
        return
      } else{
        this.refreshProgrammeSuggestions()
      }
    })
  }
}

@Pipe({
  name: 'telephone'
})
export class Telephone implements PipeTransform {

  transform(number?: string): string {
    var result = ''
    var number_array = number.match(/.{1,2}/g);
    for (var i = 0; i < number_array.length; i++){
      result += number_array[i]
      if(i != (number_array.length - 1)){
        result += '.'
      }
    }
    return result
  }
}
