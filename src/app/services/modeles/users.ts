export class UsersLight {
  id: number;
  nom: string;
  edition: number;
}

export class UsersResponse {
  error: boolean
}

export class Users {
  id: number;
  nom: string;
  edition: number;
  semaine: number;
  email: string;
  telephone: string;
  nb_jours: number;
  taux_participation: number
  participation_covoit_aller: boolean;
  participation_covoit_retour: boolean;
  conducteur: boolean;
  alcool: boolean;
  arrivee_lieu: string;
  depart_lieu: string;
  arrivee_date: string;
  depart_date: string;
  commentaire_aller: string;
  commentaire_retour: string;
  principal: boolean;
  km1: number;
  peage_aller: number;
  km2: number;
  km3: number;
  peage_retour: number;
  km4: number;
}
