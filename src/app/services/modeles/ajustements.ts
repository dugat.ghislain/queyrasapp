export class Ajustement {
  id: number;
  libelle: string;
  receveur: string;
  endette: string;
  montant: number;
}
