export class Achat {
  id: number;
  libelle: string;
  montant: number;
  alcool: boolean;
}
