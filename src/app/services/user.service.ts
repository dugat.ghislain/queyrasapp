import { Injectable } from '@angular/core';
import {Router} from '@angular/router';
import {ApiService} from './api.service';
import {CookieService} from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  user = JSON.parse(localStorage.getItem("user"))

  constructor(private apiService: ApiService, private route:Router, private cookies: CookieService) { }

  refreshUser(callback){
    if(localStorage.getItem("editionId") == null || localStorage.getItem("editionId") == "null"){

      localStorage.removeItem('edition')
      localStorage.removeItem('editionId')
      localStorage.removeItem('user')
      localStorage.removeItem('usersList')
      localStorage.removeItem('refresh_token')
      this.apiService.deleteCookies().subscribe((res) => {
        if(res == "ok"){
          this.route.navigate(['/']);
        }
        else{
          this.route.navigate(['/erreur']);
        }
      })
      return
    }
    this.apiService.getUser().subscribe((res) => {
      if(res == "create_user"){
        //TODO
        this.route.navigate(['/inscription_nouvelle_edition']);
        return
      }
      if (res == null || (res.error != null && res.error)) {
        const refresh_token = localStorage.getItem("refresh_token")
        this.apiService.resfreshToken(refresh_token).subscribe((res) => {
          if(res == "ok"){
            this.apiService.getUser().subscribe((res) => {
              if(res == "create_user"){
                //TODO
                this.route.navigate(['/inscription_nouvelle_edition']);
                return
              }
              if (res == null || (res.error != null && res.error)) {
                localStorage.removeItem('edition')
                localStorage.removeItem('editionId')
                localStorage.removeItem('user')
                localStorage.removeItem('usersList')
                localStorage.removeItem('refresh_token')
                this.apiService.deleteCookies().subscribe((res) => {
                  if(res == "ok"){
                    this.route.navigate(['/']);
                  }
                  else{
                    this.route.navigate(['/erreur']);
                  }
                })
              }
              else{
                localStorage.setItem('user', JSON.stringify(res[0]))
                callback(res[0])
              }
            })
          }
          else{
            localStorage.removeItem('edition')
            localStorage.removeItem('editionId')
            localStorage.removeItem('user')
            localStorage.removeItem('usersList')
            localStorage.removeItem('refresh_token')
            this.apiService.deleteCookies().subscribe((res) => {
              if(res == "ok"){
                this.route.navigate(['/']);
              }
              else{
                this.route.navigate(['/erreur']);
              }
            })
          }
        })

      }
      else{
        localStorage.setItem('user', JSON.stringify(res[0]))
        callback(res[0])
      }
    })
  }

}
