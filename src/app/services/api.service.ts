import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Editions} from './modeles/editions';
import {UsersLight} from './modeles/users';
import {DateModel} from './modeles/date';
import {Achat} from './modeles/achats';
import {Ajustement} from './modeles/ajustements';
import {Lieux} from './modeles/lieux';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  apiURL: string = environment.apiUrl;


  constructor(private httpClient: HttpClient) { }

  public getDefaultEdtition(){
    return this.httpClient.get<Editions[]>(`${this.apiURL}/editions/default`, {withCredentials: true});
  }

  public getUserLight(){
    return this.httpClient.get<UsersLight[]>(`${this.apiURL}/users/all/light`, {withCredentials: true});
  }

  public getUser() {
    return this.httpClient.get<any>(`${this.apiURL}/users/current`, {withCredentials: true});
  }

  public getDatesAller() {
    return this.httpClient.get<DateModel[]>(`${this.apiURL}/dates/aller/`, {withCredentials: true});
  }

  public getDatesRetour() {
    return this.httpClient.get<DateModel[]>(`${this.apiURL}/dates/retour/`, {withCredentials: true});
  }

  public getLieux(){
    return this.httpClient.get<Lieux[]>(`${this.apiURL}/lieux`, {withCredentials: true});
  }
  public getLieuxDepart(){
    return this.httpClient.get<Lieux[]>(`${this.apiURL}/lieux/depart`, {withCredentials: true});
  }

  public insertLieu(libelle: string, taux: string, depart: string){
    return this.httpClient.post<any>(`${this.apiURL}/lieux`, {libelle: libelle, taux: taux, depart: depart}, {withCredentials: true});
  }

  public getAchats(){
    return this.httpClient.get<Achat[]>(`${this.apiURL}/achats`, {withCredentials: true});
  }

  public deleteAchat(id: string){
    return this.httpClient.delete<any>(`${this.apiURL}/achats/${id}`, {withCredentials: true});
  }

  public insertAchat(libelle: string, montant: number){
    return this.httpClient.post<any>(`${this.apiURL}/achats`, {libelle: libelle, montant: montant, alcool: 0}, {withCredentials: true});
  }

  public insertAchatAlcool(user: string, libelle: string, montant: number){
    return this.httpClient.post<any>(`${this.apiURL}/achats`, {user: user, libelle: libelle, montant: montant, alcool: 1}, {withCredentials: true});
  }

  public getAjustementsReception(){
    return this.httpClient.get<Ajustement[]>(`${this.apiURL}/ajustements/reception`, {withCredentials: true});
  }

  public getAjustementsEndettement(){
    return this.httpClient.get<Ajustement[]>(`${this.apiURL}/ajustements/endettement`, {withCredentials: true});
  }

  public insertAjustement(libelle: string, receveur: string, endette: string, montant: number){
    return this.httpClient.post<any>(`${this.apiURL}/ajustements`, {libelle: libelle, receveur: receveur, endette: endette, montant: montant}, {withCredentials: true});
  }

  public deleteAjustement(id: string){
    return this.httpClient.delete<any>(`${this.apiURL}/ajustements/${id}`, {withCredentials: true});
  }

  public updateProfil(datas: any){
    return this.httpClient.post<any>(`${this.apiURL}/user/profil`, datas, {withCredentials: true});
  }

  public inscriptionNouvelleEdition(datas: any){
    return this.httpClient.post<any>(`${this.apiURL}/users/create`, datas, {withCredentials: true});
  }

  public updateAller(datas: any){
    return this.httpClient.post<any>(`${this.apiURL}/user/aller`, datas, {withCredentials: true});
  }

  public updateRetour(datas: any){
    return this.httpClient.post<any>(`${this.apiURL}/user/retour`, datas, {withCredentials: true});
  }

  public updateConducteur(datas: any){
    return this.httpClient.post<any>(`${this.apiURL}/user/conducteur`, datas, {withCredentials: true});
  }

  public submitLogin(email: string, password: string){
    return this.httpClient.post<any>(`${this.apiURL}/login`, {email: email, password: password}, {withCredentials: true});
  }

  public submitSignup(nom: string, email: string, telephone: string){
    return this.httpClient.post<any>(`${this.apiURL}/users/signup`, {nom: nom, email: email, telephone: telephone}, {withCredentials: true});
  }

  public submitPasswordForgotten(email: string){
    return this.httpClient.post<any>(`${this.apiURL}/users/password/forgot`, {email: email}, {withCredentials: true});
  }

  public getAccount(){
    return this.httpClient.get<any>(`${this.apiURL}/account`, {withCredentials: true});
  }


  public changePassword(old_password: string, new_password: string, verification: string){
    return this.httpClient.post<any>(`${this.apiURL}/users/password`, {old_password: old_password, new_password: new_password, verification: verification}, {withCredentials: true});
  }

  public resfreshToken(refresh_token: string){
    return this.httpClient.post<any>(`${this.apiURL}/tokens/refresh`, {refresh_token: refresh_token}, {withCredentials: true});
  }

  public deleteCookies(){
    return this.httpClient.get<any>(`${this.apiURL}/cookies/delete`, {withCredentials: true});
  }

  public getAllUsers(){
    return this.httpClient.get<any>(`${this.apiURL}/users/all`, {withCredentials: true});
  }

  public getPaiements(id: string){
    return this.httpClient.get<any>(`${this.apiURL}/users/${id}/paiements`, {withCredentials: true});
  }

  public addPaiement(user_id: string, montant: number){
    return this.httpClient.post<any>(`${this.apiURL}/users/paiements`, {user_id: user_id, montant: montant}, {withCredentials: true});
  }

  public updatePaiement(id: string, user_id: string, demande: number, paye: number){
    return this.httpClient.post<any>(`${this.apiURL}/users/paiements/update`, {id: id, user_id: user_id, demande: demande, paye: paye}, {withCredentials: true});
  }

  public deletePaiement(id: string, user_id: string){
    return this.httpClient.delete<any>(`${this.apiURL}/users/paiements/${user_id}/${id}`, {withCredentials: true});
  }


  public updateAllUsers(){
    return this.httpClient.get<any>(`${this.apiURL}/paiements/update`, {withCredentials: true});
  }

  public getNbJours(){
    return this.httpClient.get<any>(`${this.apiURL}/jours`, {withCredentials: true});
  }

  public getLitsUsers(){
    return this.httpClient.get<any>(`${this.apiURL}/users/lits`, {withCredentials: true});
  }

  public getLits(){
    return this.httpClient.get<any>(`${this.apiURL}/lits`, {withCredentials: true});
  }

  public getJours(){
    return this.httpClient.get<any>(`${this.apiURL}/semaines/jours`, {withCredentials: true});
  }


  public updateLit(datas){
    return this.httpClient.post<any>(`${this.apiURL}/users/lits`, datas, {withCredentials: true});
  }

  public sendEmail() {
    return this.httpClient.get<any>(`${this.apiURL}/users/send/account`, {withCredentials: true});
  }

  public getProgramme(){
    return this.httpClient.get<any>(`${this.apiURL}/programme`, {withCredentials: true});
  }

  updateProgramme(id_jour, programme_libelle: string) {
    return this.httpClient.post<any>(`${this.apiURL}/programme`, {id_jour: id_jour, libelle: programme_libelle}, {withCredentials: true});
  }

  public getProgrammeSuggestions(){
    return this.httpClient.get<any>(`${this.apiURL}/programme/suggestions`, {withCredentials: true});
  }

  insertProgrammeSuggestion(suggestion: string) {
    return this.httpClient.post<any>(`${this.apiURL}/programme/suggestions`, {suggestion: suggestion}, {withCredentials: true});
  }

  deleteSuggestion(id) {
    return this.httpClient.delete<any>(`${this.apiURL}/programme/suggestions/${id}`, {withCredentials: true});
  }
}
