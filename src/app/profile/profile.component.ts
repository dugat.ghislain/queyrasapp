import {Component, Inject, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ApiService} from '../services/api.service';
import {MatSelectChange} from '@angular/material/select';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatRadioChange} from '@angular/material/radio';
import {GoogleSheetsDbService} from 'ng-google-sheets-db';
import {UserService} from '../services/user.service';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  user = JSON.parse(localStorage.getItem("user"))
  editionId = JSON.parse(localStorage.getItem("editionId"))
  dates_aller = []
  dates_retour = []
  lieux = []
  lieu_arrivee = null;
  lieu_depart = null;
  lieu_arrivee_initial = null
  lieu_depart_initial = null
  nb_jours = null
  nb_jours_nuits = []
  conducteur = 0
  nb_jours_une_semaine = environment.nb_jours_une_semaine
  nb_jours_initial = 0
  date_arrivee_initiale = ""
  semaines = [{value:1, name: "Première"}, {value:2, name: "Deuxième"}]
  nb_jours_sejour = environment.nb_jours_sejour
  semaine = 0
  arrivee_date = ""
  clicked = false


  generalForm: FormGroup = new FormGroup({
  });

  profilForm: FormGroup = new FormGroup({
    nom: new FormControl(),
    email: new FormControl(),
    telephone: new FormControl(),
    nb_jours: new FormControl(),
    arrivee_date: new FormControl(),
    alcool: new FormControl(),
    vegetarien: new FormControl(),
    pass_sanitaire: new FormControl(),
  });

  setAllForms(){
    this.setProfilForm()
    this.setAllerForm()
    this.setRetourForm()
    this.setConducteurForm()
  }
  setProfilForm(){
    this.profilForm.get("nom").setValue(this.user.nom)
    this.profilForm.get("email").setValue(this.user.email)
    this.profilForm.get("telephone").setValue(this.user.telephone)
    this.profilForm.get("nb_jours").setValue(this.user.nb_jours)
    this.profilForm.get("arrivee_date").setValue(this.user.arrivee_date)
    this.profilForm.get("alcool").setValue(this.user.alcool.toString())
    this.profilForm.get("vegetarien").setValue(this.user.vegetarien.toString())
    this.profilForm.get("pass_sanitaire").setValue(this.user.pass_sanitaire.toString())
  }
  allerForm: FormGroup = new FormGroup({
    arrivee_lieu: new FormControl(),
    participation_covoit_aller: new FormControl(),
    commentaire_aller: new FormControl(),
  });
  setAllerForm(){
    this.allerForm.get("arrivee_lieu").setValue(this.user.arrivee_lieu)
    this.allerForm.get("participation_covoit_aller").setValue(this.user.participation_covoit_aller.toString())
    this.allerForm.get("commentaire_aller").setValue((this.user.commentaire_aller == 'null' ? '' : this.user.commentaire_aller))
  }
  retourForm: FormGroup = new FormGroup({
    depart_lieu: new FormControl(),
    participation_covoit_retour: new FormControl(),
    commentaire_retour: new FormControl(),
  });
  setRetourForm(){
    this.retourForm.get("depart_lieu").setValue(this.user.depart_lieu)
    this.retourForm.get("participation_covoit_retour").setValue(this.user.participation_covoit_retour.toString())
    this.retourForm.get("commentaire_retour").setValue((this.user.commentaire_retour == 'null' ? '' : this.user.commentaire_retour))
  }
  conducteurForm: FormGroup = new FormGroup({
    conducteur: new FormControl(),
    nb_places: new FormControl(),
    nb_places_covoit: new FormControl(),
    km1: new FormControl(),
    km2: new FormControl(),
    km3: new FormControl(),
    km4: new FormControl(),
    peage_aller: new FormControl(),
    peage_retour: new FormControl(),
  });
  setConducteurForm(){
    this.conducteurForm.get("conducteur").setValue(this.user.conducteur.toString())
    this.conducteurForm.get("nb_places").setValue(this.user.nb_places)
    this.conducteurForm.get("nb_places_covoit").setValue(this.user.nb_places_covoit)
    this.conducteurForm.get("km1").setValue(this.user.km1)
    this.conducteurForm.get("km2").setValue(this.user.km2)
    this.conducteurForm.get("km3").setValue(this.user.km3)
    this.conducteurForm.get("km4").setValue(this.user.km4)
    this.conducteurForm.get("peage_aller").setValue(this.user.peage_aller)
    this.conducteurForm.get("peage_retour").setValue(this.user.peage_retour)
  }


  constructor(private apiService: ApiService, private userService: UserService, public dialog: MatDialog) { }


  ngOnInit(): void {
    this.updateUser(() => {
      this.setAllForms()
      this.lieu_arrivee = this.user.arrivee_lieu
      this.lieu_depart = this.user.depart_lieu
      this.nb_jours = this.user.nb_jours
      this.nb_jours_initial = this.user.nb_jours
      this.date_arrivee_initiale = this.user.arrivee_date
      this.conducteur = this.user.conducteur
      this.refreshLieux(true, () => {})
      this.apiService.getDatesAller().subscribe((res)=>{
        for (var i = 0; i < res.length; i++){
          this.dates_aller[i] = res[i].date
        }
        this.dates_aller.sort((a,b) => {
          var a_parts = a.split("/");
          var a_dt = new Date(parseInt(a_parts[2], 10),
            parseInt(a_parts[1], 10) - 1,
            parseInt(a_parts[0], 10));
          var b_parts = b.split("/");
          var b_dt = new Date(parseInt(b_parts[2], 10),
            parseInt(b_parts[1], 10) - 1,
            parseInt(b_parts[0], 10));
          if(a_dt.getTime()<b_dt.getTime()){
            return -1
          }else if(b_dt.getTime()<a_dt.getTime()){
            return 1
          }
          return 0;
        })
      })
      this.apiService.getDatesRetour().subscribe((res)=>{
        for (var i = 0; i < res.length; i++){
          this.dates_retour[i] = res[i].date
        }
        this.dates_retour.sort((a,b) => {
          var a_parts = a.split("/");
          var a_dt = new Date(parseInt(a_parts[2], 10),
            parseInt(a_parts[1], 10) - 1,
            parseInt(a_parts[0], 10));
          var b_parts = b.split("/");
          var b_dt = new Date(parseInt(b_parts[2], 10),
            parseInt(b_parts[1], 10) - 1,
            parseInt(b_parts[0], 10));
          if(a_dt.getTime()<b_dt.getTime()){
            return -1
          }else if(b_dt.getTime()<a_dt.getTime()){
            return 1
          }
          return 0;
        })
        this.apiService.getNbJours().subscribe((res)=>{
          this.nb_jours_nuits = res
          console.log(this.nb_jours_nuits)
          this.nb_jours_nuits.sort((a,b) => {
            if(a < b) return -1
            else return 1
            return 0;
          })
        })
      })

    })
  }

  updateUser(callback){
    this.userService.refreshUser((user) => {
      this.user = user
      this.nb_jours = user.nb_jours
      this.semaine = user.semaine
      this.arrivee_date = user.arrivee_date
      this.setAllForms()
      callback()
    })
  }

  onEditionArrivee($event: MatSelectChange) {
    var lieu = $event.value
    if(lieu == 'other'){
      const dialogRef = this.dialog.open(PopupLieu, {
        width: '250px'
      });
      dialogRef.afterClosed().subscribe(result => {
        if(result){
          this.refreshLieux(false, () => {
            this.allerForm.get("arrivee_lieu").setValue(result.lieu)
            this.lieu_arrivee = result.lieu
            this.lieu_arrivee_initial = result.depart
          })

        }
        else
          $event.source.value = this.lieu_arrivee
      });
    }else{
      this.lieu_arrivee = lieu
      for (var i = 0; i < this.lieux.length; i++){
        if(this.lieux[i].libelle == lieu){
          this.lieu_arrivee_initial = this.lieux[i].depart
        }
      }
    }
  }

  onEditionDepart($event: MatSelectChange) {
    var lieu = $event.value
    if(lieu == 'other'){
      const dialogRef = this.dialog.open(PopupLieu, {
        width: '250px'
      });
      dialogRef.afterClosed().subscribe(result => {
        if(result){
          this.refreshLieux(false, () => {
            this.allerForm.get("depart_lieu").setValue(result.lieu)
            this.lieu_depart = result.lieu
            this.lieu_depart_initial = result.depart
          })
        }
        else
          $event.source.value = this.lieu_depart
      });
    }else{
      this.lieu_depart = lieu
      for (var i = 0; i < this.lieux.length; i++){
        if(this.lieux[i].libelle == lieu){
          this.lieu_depart_initial = this.lieux[i].depart
        }
      }
    }
  }

  refreshLieux(refreshAll: boolean, callback){
    this.apiService.getLieux().subscribe((res)=>{
      this.lieux = res
      callback()
      if(refreshAll){
        for (var i = 0; i < this.lieux.length; i++){
          if(this.lieux[i].libelle == this.user.arrivee_lieu){
            this.lieu_arrivee_initial = this.lieux[i].depart
          }
          if(this.lieux[i].libelle == this.user.depart_lieu){
            this.lieu_depart_initial = this.lieux[i].depart
          }
        }
      }

    })
  }

  onEditionNbJours($event: MatSelectChange) {
    if($event.value <= this.nb_jours_une_semaine){
      this.profilForm.get('nb_jours').setValidators([Validators.required, Validators.min(1), Validators.max(2)]);
      this.profilForm.get('nb_jours').updateValueAndValidity();
    }else{
      this.profilForm.get('nb_jours').clearValidators();
      this.profilForm.get('nb_jours').updateValueAndValidity();
    }
    this.changeSemaine()
    this.nb_jours = $event.value
  }

  onEditionArriveeDate() {
    this.changeSemaine()
    this.arrivee_date = this.profilForm.get('arrivee_date').value
  }

  changeSemaine(){
    var semaine = 0
    if(this.profilForm.get('nb_jours').value <= this.nb_jours_une_semaine){
      var date_retour_object = this.getDateRetourCalculee(this.getNuitFromJour(this.profilForm.get('nb_jours').value), this.profilForm.get('arrivee_date').value)
      var datePartsMiddle = environment.jour_semaine_1_2.split("/");
      var mounthMiddle = parseInt(datePartsMiddle[1])

      var dateObjectMiddle = new Date(+datePartsMiddle[2], mounthMiddle - 1, +datePartsMiddle[0], 0,0,0,0);
      if(date_retour_object <= dateObjectMiddle) semaine = 1
      else semaine = 2
    }
    this.semaine = semaine
  }

  conducteurChange(event) {
    console.log(event.value)
    this.conducteur = this.conducteurForm.get('conducteur').value
  }

  submitProfil() {
    if(this.clicked) return
    this.clicked = true
    if(this.profilForm.get("nom").value == null || this.profilForm.get("nom").value == ""){
      this.clicked = false
      alert("Tu n'as pas mis ton blaze !")
      return
    }
    if(this.profilForm.get("email").value == null || this.profilForm.get("email").value == "" ||
      !this.profilForm.get("email").value.match("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")){
      this.clicked = false
      alert("Email invalide !")
      return
    }
    if(this.profilForm.get("telephone").value == null || this.profilForm.get("telephone").value == "" ||
      !this.profilForm.get("telephone").value.match("^(0)[1-9](\\d{2}){4}$")){
      this.clicked = false
      alert("Téléphone invalide !")
      return
    }
    var nb_jours = this.profilForm.get("nb_jours").value
    var arrivee_date = this.profilForm.get("arrivee_date").value

    var date_retour_object = this.getDateRetourCalculee(this.getNuitFromJour(nb_jours), arrivee_date)
    var date_retour = date_retour_object.toLocaleDateString("fr-FR")
    if(this.dates_retour.indexOf(date_retour) == -1){
      this.clicked = false
      alert("Le nombre de jours n'est pas bon !")
      return
    }

    var semaine = 0
    if(nb_jours <= this.nb_jours_une_semaine){
      var datePartsMiddle = environment.jour_semaine_1_2.split("/");
      var mounthMiddle = parseInt(datePartsMiddle[1])

      var dateObjectMiddle = new Date(+datePartsMiddle[2], mounthMiddle - 1, +datePartsMiddle[0], 0,0,0,0);
      if(date_retour_object <= dateObjectMiddle) semaine = 1
      else semaine = 2
    }

    var datas = {
      nom: this.profilForm.get("nom").value,
      email: this.profilForm.get("email").value,
      telephone: this.profilForm.get("telephone").value,
      nb_jours: nb_jours,
      semaine: semaine,
      alcool: this.profilForm.get("alcool").value,
      vegetarien: this.profilForm.get("vegetarien").value,
      pass_sanitaire: this.profilForm.get("pass_sanitaire").value,
      arrivee_date: arrivee_date,
    }
    this.apiService.updateProfil(datas).subscribe((res)=>{
      if(res != "ok"){
        this.clicked = false
        alert(res)
      }else{
        this.nb_jours_initial = nb_jours
        this.date_arrivee_initiale = arrivee_date
        this.submitAller()
      }
    })
  }

  checkNbJours(nb_jours: any) {
    var arrivee_date = this.profilForm.get("arrivee_date").value
    var date_retour = this.getDateRetourCalculee(nb_jours, arrivee_date).toLocaleDateString("fr-FR")
    return this.dates_retour.indexOf(date_retour) != -1;
  }

  checkDisplaySemaine(){
    var nb_jours = this.profilForm.get("nb_jours").value
    var arrivee_date = this.profilForm.get("arrivee_date").value


    if(arrivee_date == null){
      return false
    }

    var date_retour_object = this.getDateRetourCalculee(this.getNuitFromJour(nb_jours), arrivee_date)
    var date_retour = date_retour_object.toLocaleDateString("fr-FR")
    return this.dates_retour.indexOf(date_retour) != -1
  }

  getNuitFromJour(nbJour){
    for (let nbJoursKey of this.nb_jours_nuits) {
      if(nbJoursKey.nb_jours == nbJour) return nbJoursKey.nb_nuits
    }
    return null
  }

  getDateRetourCalculee(nb_jours, date_arrivee){
    var dateParts = date_arrivee.split("/");

    var dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0], 0,0,0,0);

    dateObject.setDate(dateObject.getDate() + nb_jours);
    return dateObject
  }

  submitAller() {
    if(this.allerForm.get("arrivee_lieu").value == null){
      alert("Tu n'as pas renseigné ton lieu de départ.")
      this.clicked = false
      return
    }
    var participation_covoit = (this.lieu_arrivee_initial == "Aix-Marseille" ? "1" : this.allerForm.get("participation_covoit_aller").value)
    var datas = {
      arrivee_lieu: this.allerForm.get("arrivee_lieu").value,
      participation_covoit_aller: participation_covoit,
      commentaire_aller: (this.allerForm.get("commentaire_aller").value ? this.allerForm.get("commentaire_aller").value : ''),
    }
    this.apiService.updateAller(datas).subscribe((res)=>{
      if(res != "ok"){
        this.clicked = false
        alert(res)
      }else{
        this.submitRetour()
      }
    })
  }
  submitRetour() {
    if(this.retourForm.get("depart_lieu").value == null){
      alert("Tu n'as pas renseigné ton lieu d'arrivée.")
      this.clicked = false
      return
    }
    var participation_covoit = (this.lieu_depart_initial == "Aix-Marseille" ? "1" : this.retourForm.get("participation_covoit_retour").value)
    var datas = {
      depart_lieu: this.retourForm.get("depart_lieu").value,
      participation_covoit_retour: participation_covoit,
      commentaire_retour: (this.retourForm.get("commentaire_retour").value ? this.retourForm.get("commentaire_retour").value : ''),
    }
    this.apiService.updateRetour(datas).subscribe((res)=>{
      if(res != "ok"){
        this.clicked = false
        alert(res)
      }else{
        this.submitConducteur()
      }
    })
  }
  submitConducteur() {
    var datas = {
      conducteur: this.conducteurForm.get("conducteur").value,
      nb_places: this.conducteurForm.get("nb_places").value,
      nb_places_covoit: this.conducteurForm.get("nb_places_covoit").value,
      km1: (this.conducteurForm.get("km1").value ? this.conducteurForm.get("km1").value : 0),
      km2: (this.conducteurForm.get("km2").value ? this.conducteurForm.get("km2").value : 0),
      km3: (this.conducteurForm.get("km3").value ? this.conducteurForm.get("km3").value : 0),
      km4: (this.conducteurForm.get("km4").value ? this.conducteurForm.get("km4").value : 0),
      peage_aller: (this.conducteurForm.get("peage_aller").value ? this.conducteurForm.get("peage_aller").value : 0),
      peage_retour: (this.conducteurForm.get("peage_retour").value ? this.conducteurForm.get("peage_retour").value : 0),
    }
    this.apiService.updateConducteur(datas).subscribe((res)=>{
      if(res != "ok"){
        alert(res)
      }else{
        this.updateUser(() => {alert("C'est enregistré !")})
      }
      this.clicked = false
    })
  }


}

@Component({
  selector: 'popup-lieu',
  templateUrl: 'popup-lieu.html',
})
export class PopupLieu {

  lieux = null
  lieu_selectionne = null

  lieuForm: FormGroup = new FormGroup({
    libelle: new FormControl(),
    taux: new FormControl(),
    depart: new FormControl()
  });

  constructor(
    public dialogRef: MatDialogRef<PopupLieu>, private apiService: ApiService){
    this.apiService.getLieuxDepart().subscribe((res)=>{
      this.lieux = res
    })
  }


  onClosePopup(): void {
    this.dialogRef.close();
  }


  submitCreationLieu() {
    var lieu = this.lieuForm.get('libelle').value
    var taux = this.lieuForm.get('taux').value
    var depart = this.lieuForm.get('depart').value
    if(lieu == null || lieu == "" ||
      taux == null || taux == "0" ||
      depart == null || depart == ""){
      alert("Faut tout renseigner !")
    }
    else{
      if(depart = 'same_as_lieu'){
        depart = lieu
      }
      lieu = this.titleCaseWord(lieu)
      depart = this.titleCaseWord(depart)
      this.apiService.insertLieu(lieu, taux, depart).subscribe((res)=>{
        if(res != "ok"){
          alert(res)
        }else{
            this.dialogRef.close({lieu: lieu, depart: depart});
        }
      })

    }

  }
  titleCaseWord(word: string) {
    if (!word) return word;
    return word[0].toUpperCase() + word.substr(1).toLowerCase();
  }
}
