import {Component, Input, OnInit, TemplateRef} from '@angular/core';
import {Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';
import {ApiService} from '../services/api.service';


@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  @Input()
  mainContent: TemplateRef<any>;

  edition = (localStorage.getItem("edition") == null || localStorage.getItem("editionId") == "null") ? 'Queyras App' : localStorage.getItem("edition")

  router = this.route
  constructor(private apiService: ApiService, private route:Router, private cookies: CookieService) { }

  ngOnInit(): void {
    if(localStorage.getItem("user") == null || localStorage.getItem("editionId") == null){
      localStorage.removeItem('edition')
      localStorage.removeItem('editionId')
      localStorage.removeItem('user')
      localStorage.removeItem('usersList')
      localStorage.removeItem('refresh_token')
      this.cookies.delete('authtoken')
      this.route.navigate(['/']);
    }
  }

  logout() {
    localStorage.removeItem('edition')
    localStorage.removeItem('editionId')
    localStorage.removeItem('user')
    localStorage.removeItem('usersList')
    localStorage.removeItem('refresh_token')
    this.apiService.deleteCookies().subscribe((res) => {
      if(res == "ok"){
        this.route.navigate(['/']);
      }
      else{
        this.route.navigate(['/erreur']);
      }
    })
  }

}
