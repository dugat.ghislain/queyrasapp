import { Component, OnInit } from '@angular/core';
import {environment} from '../../environments/environment';
import {ApiService} from '../services/api.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSelectChange} from '@angular/material/select';
import {Router} from '@angular/router';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {

  dates_aller = []
  dates_retour = []
  nb_jours = null
  nb_jours_nuits = []
  nb_jours_une_semaine = environment.nb_jours_une_semaine
  nb_jours_initial = 0
  date_arrivee_initiale = ""
  semaines = [{value:1, name: "Première"}, {value:2, name: "Deuxième"}]
  semaine = 0
  arrivee_date = ""
  clicked = false
  charteUrl = environment.lienCharte
  users_list = JSON.parse(localStorage.getItem("usersList"))
  nb_semaine_1 = 0
  nb_semaine_2 = 0

  profilForm: FormGroup = new FormGroup({
    nb_jours: new FormControl(),
    arrivee_date: new FormControl(),
    alcool: new FormControl(),
    vegetarien: new FormControl(),
    pass_sanitaire: new FormControl(),
    luEtAccepte: new FormControl(),
    luEtAccepteMontant: new FormControl(),
    luEtAccepteDraps: new FormControl(),
  });


  constructor(private apiService: ApiService, private route:Router) { }

  ngOnInit(): void {
    if(localStorage.getItem("editionId") == null){
      this.route.navigate(['/']);
      return
    }
    this.apiService.getDatesAller().subscribe((res)=>{
      for (var i = 0; i < res.length; i++){
        this.dates_aller[i] = res[i].date
      }
      this.dates_aller.sort((a,b) => {
        var a_parts = a.split("/");
        var a_dt = new Date(parseInt(a_parts[2], 10),
          parseInt(a_parts[1], 10) - 1,
          parseInt(a_parts[0], 10));
        var b_parts = b.split("/");
        var b_dt = new Date(parseInt(b_parts[2], 10),
          parseInt(b_parts[1], 10) - 1,
          parseInt(b_parts[0], 10));
        if(a_dt.getTime()<b_dt.getTime()){
          return -1
        }else if(b_dt.getTime()<a_dt.getTime()){
          return 1
        }
        return 0;
      })
    })
    this.apiService.getDatesRetour().subscribe((res)=>{
      for (var i = 0; i < res.length; i++){
        this.dates_retour[i] = res[i].date
      }
      this.dates_retour.sort((a,b) => {
        var a_parts = a.split("/");
        var a_dt = new Date(parseInt(a_parts[2], 10),
          parseInt(a_parts[1], 10) - 1,
          parseInt(a_parts[0], 10));
        var b_parts = b.split("/");
        var b_dt = new Date(parseInt(b_parts[2], 10),
          parseInt(b_parts[1], 10) - 1,
          parseInt(b_parts[0], 10));
        if(a_dt.getTime()<b_dt.getTime()){
          return -1
        }else if(b_dt.getTime()<a_dt.getTime()){
          return 1
        }
        return 0;
      })
      this.apiService.getNbJours().subscribe((res)=>{
        this.nb_jours_nuits = res
        console.log(this.nb_jours_nuits)
        this.nb_jours_nuits.sort((a,b) => {
          if(a < b) return -1
          else return 1
          return 0;
        })
      })
    })

    this.users_list = JSON.parse(localStorage.getItem("usersList"))
    for (var i = 0; i < this.users_list.length; i++){
      if(this.users_list[i].semaine == 0 || this.users_list[i].semaine == 1){
        this.nb_semaine_1 += 1
      }
      if(this.users_list[i].semaine == 0 || this.users_list[i].semaine == 2){
        this.nb_semaine_2 += 1
      }
    }
  }

  onEditionArriveeDate() {
    this.changeSemaine()
    this.arrivee_date = this.profilForm.get('arrivee_date').value
  }

  changeSemaine(){
    var semaine = 0
    if(this.profilForm.get('nb_jours').value <= this.nb_jours_une_semaine){
      var date_retour_object = this.getDateRetourCalculee(this.getNuitFromJour(this.profilForm.get('nb_jours').value), this.profilForm.get('arrivee_date').value)
      var datePartsMiddle = environment.jour_semaine_1_2.split("/");
      var mounthMiddle = parseInt(datePartsMiddle[1])

      var dateObjectMiddle = new Date(+datePartsMiddle[2], mounthMiddle - 1, +datePartsMiddle[0], 0,0,0,0);
      if(date_retour_object <= dateObjectMiddle) semaine = 1
      else semaine = 2
    }
    this.semaine = semaine
  }

  checkNbJours(nb_jours: any) {
    var arrivee_date = this.profilForm.get("arrivee_date").value
    var date_retour = this.getDateRetourCalculee(nb_jours, arrivee_date).toLocaleDateString("fr-FR")
    return this.dates_retour.indexOf(date_retour) != -1;
  }

  getNuitFromJour(nbJour){
    for (let nbJoursKey of this.nb_jours_nuits) {
      if(nbJoursKey.nb_jours == nbJour) return nbJoursKey.nb_nuits
    }
    return null
  }

  getDateRetourCalculee(nb_jours, date_arrivee){
    var dateParts = date_arrivee.split("/");

    var dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0], 0,0,0,0);

    dateObject.setDate(dateObject.getDate() + nb_jours);
    return dateObject
  }

  onEditionNbJours($event: MatSelectChange) {
    if($event.value <= this.nb_jours_une_semaine){
      this.profilForm.get('nb_jours').setValidators([Validators.required, Validators.min(1), Validators.max(2)]);
      this.profilForm.get('nb_jours').updateValueAndValidity();
    }else{
      this.profilForm.get('nb_jours').clearValidators();
      this.profilForm.get('nb_jours').updateValueAndValidity();
    }
    this.changeSemaine()
    this.nb_jours = $event.value
  }

  checkDisplaySemaine(){
    var nb_jours = this.profilForm.get("nb_jours").value
    var arrivee_date = this.profilForm.get("arrivee_date").value


    if(arrivee_date == null){
      return false
    }

    var date_retour_object = this.getDateRetourCalculee(this.getNuitFromJour(nb_jours), arrivee_date)
    var date_retour = date_retour_object.toLocaleDateString("fr-FR")
    return this.dates_retour.indexOf(date_retour) != -1
  }

  submitProfil() {
    if(this.clicked) return
    this.clicked = true
    var nb_jours = this.profilForm.get("nb_jours").value
    var arrivee_date = this.profilForm.get("arrivee_date").value

    if(!this.profilForm.get("luEtAccepte").value){
      alert("Tu n'as pas accepté la charte de conduite.")
      this.clicked = false
      return
    }
    if(!this.profilForm.get("luEtAccepteMontant").value){
      alert("Tu ne t'es pas engagé à rembourser le prix du séjour.")
      this.clicked = false
      return
    }
    if(!this.profilForm.get("luEtAccepteDraps").value){
      alert("Tu n'as pas confirmé que tu es courant pour les draps et les vêtements.")
      this.clicked = false
      return
    }

    var date_retour_object = this.getDateRetourCalculee(this.getNuitFromJour(nb_jours), arrivee_date)
    var date_retour = date_retour_object.toLocaleDateString("fr-FR")
    if(this.dates_retour.indexOf(date_retour) == -1){
      this.clicked = false
      alert("Le nombre de jours n'est pas bon !")
      return
    }

    var semaine = 0
    if(nb_jours <= this.nb_jours_une_semaine){
      var datePartsMiddle = environment.jour_semaine_1_2.split("/");
      var mounthMiddle = parseInt(datePartsMiddle[1])

      var dateObjectMiddle = new Date(+datePartsMiddle[2], mounthMiddle - 1, +datePartsMiddle[0], 0,0,0,0);
      if(date_retour_object <= dateObjectMiddle) semaine = 1
      else semaine = 2
    }

    var datas = {
      nb_jours: nb_jours,
      semaine: semaine,
      alcool: this.profilForm.get("alcool").value,
      vegetarien: this.profilForm.get("vegetarien").value,
      pass_sanitaire: this.profilForm.get("pass_sanitaire").value,
      arrivee_date: arrivee_date,
    }
    this.apiService.inscriptionNouvelleEdition(datas).subscribe((res)=>{
      if(res != "ok"){
        alert(res)
      }else{
        this.route.navigate(['/mon-profil']);

      }
      this.clicked = false
    })
  }

}
