import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AjustementsComponent } from './ajustements.component';

describe('AjustementsComponent', () => {
  let component: AjustementsComponent;
  let fixture: ComponentFixture<AjustementsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AjustementsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AjustementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
