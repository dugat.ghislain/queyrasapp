import { Component, OnInit } from '@angular/core';
import {ApiService} from '../services/api.service';
import {Router} from '@angular/router';
import {FormArray, FormControl, FormGroup} from '@angular/forms';
import {UserService} from '../services/user.service';
import {MatCheckbox, MatCheckboxChange} from '@angular/material/checkbox';

@Component({
  selector: 'app-ajustements',
  templateUrl: './ajustements.component.html',
  styleUrls: ['./ajustements.component.css']
})
export class AjustementsComponent implements OnInit {

  user = JSON.parse(localStorage.getItem("user"))
  clicked = false
  users_list = JSON.parse(localStorage.getItem("usersList"));
  editionId = localStorage.getItem("editionId")
  ajustements_reception = []
  ajustements_endettement = []
  receptionForm: FormGroup = new FormGroup({
    endettes: new FormArray([]),
    libelle: new FormControl(),
    montant_reception: new FormControl(),
  });
  montant_reception = 0;
  endettementForm: FormGroup = new FormGroup({
    receveur: new FormControl(),
    libelle: new FormControl(),
    montant_endettement: new FormControl(),
  });
  montant_endettement = 0;
  ajustements_reception_total = 0.00
  ajustements_endettement_total = 0.00
  customReception = false

  constructor(private apiService: ApiService, private userService: UserService) { }

  ngOnInit(): void {
    this.userService.refreshUser((user) => {
      this.user = user
      this.refreshAjustementsReception()
      this.refreshAjustementsEndettement()
    })
    this.refreshUsersList()
  }

  refreshUsersList() {
    this.apiService.getUserLight().subscribe((res)=>{
      localStorage.setItem('usersList', JSON.stringify(res))
      this.users_list = res;
      for (const usersListElement of this.users_list) {
        usersListElement.reception = {montant: 0, custom: false}
      }

    })
  }

  refreshAjustementsReception(){
    this.apiService.getAjustementsReception().subscribe((res) => {
      this.ajustements_reception = res
      this.ajustements_reception_total = 0.00
      for (const ajustement of this.ajustements_reception) {
        this.ajustements_reception_total += parseFloat(ajustement.montant)
      }
    })
  }

  refreshAjustementsEndettement(){
    this.apiService.getAjustementsEndettement().subscribe((res) => {
      this.ajustements_endettement = res
      this.ajustements_endettement_total = 0.00
      for (const ajustement of this.ajustements_endettement) {
        this.ajustements_endettement_total += parseFloat(ajustement.montant)
      }
    })
  }

  supprimerAjustement(event, id) {
    if(this.clicked) return
    this.clicked = true
    if(confirm("T'es sûr de vouloir supprimer cet dette ?")) {
      this.apiService.deleteAjustement(id).subscribe((res) => {
        if(res != "ok"){
          alert("erreur")
        }
        else{
          this.refreshAjustementsReception()
        }
        this.clicked = false
      })
    } else{
      this.clicked = false
    }
  }


  onCheckChange(event: MatCheckboxChange) {
    if(this.customReception) return
    const formArray: FormArray = this.receptionForm.get('endettes') as FormArray;
    //
    /* Selected */
    if(event.checked){
      // Add a new control in the arrayForm
      formArray.push(new FormControl(event.source.value));
    }
    /* unselected */
    else{
      // find the unselected element
      let i: number = 0;

      formArray.controls.forEach((ctrl: FormControl) => {
        if(ctrl.value == event.source.value) {
          // Remove the unselected element from the arrayForm
          formArray.removeAt(i);
          return;
        }

        i++;
      });
    }
    this.refreshReceptionInputs()
  }


  inputPriceChange(event: Event, id, oldMontant) {
    const target = event.target as HTMLInputElement
    let montantString = target.value
    let montant = Math.round((parseFloat(montantString.toString()) + Number.EPSILON) * 100) / 100
    if(montant <= 0.00){
      target.value = oldMontant
      return
    }

    const userToUpdate = this.users_list.filter(x => (x.id === id))[0]
    userToUpdate.reception.custom = true
    userToUpdate.reception.montant = montant
    this.refreshReceptionInputs()
  }

  montantNonCustom = 0.0
  montantCustom = 0.0
  refreshReceptionInputs(){
    const formArray: FormArray = this.receptionForm.get('endettes') as FormArray;
    const usersToUpdate = formArray.value
    let usersToUpdateNonCustom = [];
    this.montantCustom = 0.00
    this.users_list.forEach((userUpdated) => {
      if(usersToUpdate.findIndex(element => element === userUpdated.id) != -1){
        if(userUpdated.reception.custom){
          this.montantCustom = this.montantCustom + parseFloat(userUpdated.reception.montant)
        } else{
          usersToUpdateNonCustom.push(userUpdated.id)
        }
      }
      else {
        userUpdated.reception.montant = 0
        userUpdated.reception.custom = false
      }
    })
    this.montantNonCustom = (parseFloat(this.montant_reception.toString())-parseFloat(this.montantCustom.toString()))

    this.users_list.forEach((userUpdated) => {
      if(usersToUpdateNonCustom.findIndex(element => element === userUpdated.id) != -1){
        userUpdated.reception.montant = Math.round((( this.montantNonCustom / parseFloat(usersToUpdateNonCustom.length.toString())) + Number.EPSILON) * 100) / 100
      }
    })
  }
  submitAjustementReception() {
    if(this.clicked) return
    this.clicked = true
    var libelle = this.titleCaseWord(this.receptionForm.get('libelle').value)
    var endette = this.receptionForm.get('endettes').value
    if(libelle == null || libelle == "" || endette == null || endette == "" || this.montant_reception == 0 ){
      alert('Il y a des erreurs (libelle ou montants vide)')
      this.clicked = false
      return;
    }

    if((this.montantCustom + this.montantNonCustom) != this.montant_reception){
      alert('Les somme des remboursements demandés ne correspond pas au total demandé.')
      this.clicked = false
      return;
    }

    let endettesToUpdate = this.receptionForm.get('endettes').value

    this.insertAjustementRécursif(0, endettesToUpdate, libelle, this.users_list.filter(x => (x.id === endettesToUpdate[0]))[0].reception.montant)
  }

  insertAjustementRécursif(i, endettesToUpdate, libelle, montant){


    const endette = endettesToUpdate[i]
    if(endette != this.user.id){


      this.apiService.insertAjustement(libelle, this.user.id, endette, montant).subscribe((res) => {
        if(res != 'ok'){
          alert("Une erreur s'est produite, merci de rafraichier la page pour vérifier tes montants.")
          this.finInsertAjustementRecursif()
        } else{
          i++
          if(i == endettesToUpdate.length){
            this.finInsertAjustementRecursif()
          } else{
            this.insertAjustementRécursif(i, endettesToUpdate, libelle, this.users_list.filter(x => (x.id === endettesToUpdate[i]))[0].reception.montant)
          }
        }
      })
    }
    else{
      i++
      if(i == endettesToUpdate.length){
        this.finInsertAjustementRecursif()
      } else{
        this.insertAjustementRécursif(i, endettesToUpdate, libelle, this.users_list.filter(x => (x.id === endettesToUpdate[i]))[0].reception.montant)
      }
    }
  }

  finInsertAjustementRecursif(){
    this.receptionForm.get('libelle').setValue("")
    this.receptionForm.get('montant_reception').setValue(0)
    let endettesArray = this.receptionForm.get('endettes') as FormArray
    while (endettesArray.length !== 0) {
      endettesArray.removeAt(0)
    }
    this.refreshAjustementsReception()
    this.refreshUsersList()
    this.clicked = false
  }

  submitAjustementEndettement() {
    if(this.clicked) return
    this.clicked = true
    var libelle = this.titleCaseWord(this.endettementForm.get('libelle').value)
    var receveur = this.endettementForm.get('receveur').value
    if(libelle == null || libelle == "" || receveur == null || receveur == "" || this.montant_endettement == 0 ){
      alert('Il y a des erreurs (libelle ou montants vide)')
      this.clicked = false
      return;
    }

    this.apiService.insertAjustement(libelle, receveur, this.user.id, this.montant_endettement).subscribe((res) => {
      if(res != "ok"){
        alert("erreur")
      }
      else{
        this.endettementForm.get('libelle').setValue("")
        this.endettementForm.get('montant_endettement').setValue(0)
        this.endettementForm.get('receveur').setValue("")
        this.refreshAjustementsEndettement()
      }
      this.clicked = false
    })
  }


  titleCaseWord(word: string) {
    if (!word) return word;
    return word[0].toUpperCase() + word.substr(1).toLowerCase();
  }

  checkIfUserInReception(id) {
    return this.receptionForm.get('endettes').value.findIndex((element) => element === id) != -1
  }
}
