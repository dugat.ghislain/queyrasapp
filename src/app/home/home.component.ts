import {Component, NgModule, OnInit, ViewChild} from '@angular/core';
import {ApiService} from '../services/api.service';
import {Router} from '@angular/router';
import {FormControl, FormGroup} from '@angular/forms';
import {UserService} from '../services/user.service';
import {environment} from '../../environments/environment';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  userStored = null;
  users = null;
  default_edition = null;
  change_password = false;

  loginForm: FormGroup = new FormGroup({
    email: new FormControl(),
    password: new FormControl(),
  });

  signupForm: FormGroup = new FormGroup({
    nom: new FormControl(),
    email: new FormControl(),
    telephone: new FormControl(),
  });


  changePasswordForm: FormGroup = new FormGroup({
    oldPassword: new FormControl(),
    newPassword: new FormControl(),
    verification: new FormControl(),
  });

  displayForms = false


  constructor(private apiService: ApiService, private userService: UserService, private route:Router) { }

  ngOnInit(): void {
    if(this.userStored == null){
      this.apiService.getAccount().subscribe((account)=>{
        this.getAccount(false, account)
      })
    }
    else if(localStorage.getItem("editionId") != null && localStorage.getItem("editionId") != "null"){
      this.route.navigate(['/mon-profil']);
    }
  }


  submitLogin() {
    this.apiService.submitLogin(this.loginForm.get("email").value, this.loginForm.get("password").value).subscribe((res)=>{
      if(res == null || res.error){
        alert('Mauvais login/mot de passe.')
        return
      }
      console.log(res)
      localStorage.setItem('refresh_token', res.tokens.refresh_token)
      this.getAccount(true, res)
    })
  }


  submitSignup() {
    this.apiService.submitSignup(this.signupForm.get("nom").value, this.signupForm.get("email").value, this.signupForm.get("telephone").value).subscribe((res)=>{
      if(res == null){
        alert("Tu n'as pas bien rempli tes infos.")
        return
      }
      if(res.error){
        alert(res.message)
        return
      }
      if(res != "ok"){
        alert(res)
        return
      }
      alert("Tu es bien inscrit. Vérifie tes emails pour pouvoir te connecter.")
      return
    })
  }

  getAccount(displayMessage: boolean, account: any){
    if(account == null || account.error){
      if (displayMessage) alert('Mauvais login/mot de passe.')
      this.displayForms = true
      return
    }
    if(account.activated == 0){
      alert('Il faut activer ton compte !')
    } else if(account.change_password == 1){
      this.displayForms = true
      this.change_password = true
    } else if(account.has_user == 0){
      //TODO POUR L'ANNEE PROCHAINE
    } else {
      this.redirectToProfil()
    }
  }


  submitChangePassword() {
    var old_password = this.changePasswordForm.get("oldPassword").value
    var new_password = this.changePasswordForm.get("newPassword").value
    var verification = this.changePasswordForm.get("verification").value

    if(old_password == null || new_password == null || verification == null){
      alert('Il manque un mot de passe !')
      return
    }
    if(new_password !== verification){
      alert("Le mot de passe de vérification n'est pas bon !")
      return
    }
    let strongPassword = new RegExp('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})')
    if(!strongPassword.test(new_password)) {
      alert("Le nouveau mot de passe n'est pas bon (respect des règles du mot de passe) !")
      return
    }
    this.apiService.changePassword(old_password, new_password, verification).subscribe((res)=>{
      if(res.error){
        alert(res.message)
        return
      }
      this.redirectToProfil()
    })
  }

  redirectToProfil(){
    this.apiService.getDefaultEdtition().subscribe((res)=>{
      if(res == null || res.length == 0){
        this.route.navigate(['/erreur']);
      }
      this.default_edition = res[0].nom

      localStorage.setItem('edition', res[0].nom)
      localStorage.setItem('editionId', res[0].annee.toString())
      if(this.default_edition != null){
        this.apiService.getUserLight().subscribe((res)=>{
          localStorage.setItem('usersList', JSON.stringify(res))
          this.users = res;
        })
      }
      this.userService.refreshUser((user) => {
        if(localStorage.getItem("editionId") != null && localStorage.getItem("editionId") != "null") {
          this.route.navigate(['/mon-profil']);
        }
      })
    })

  }

}
