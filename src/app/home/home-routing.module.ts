import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import {ProfileComponent} from '../profile/profile.component';
import {ErreurComponent} from '../erreur/erreur.component';
import {AchatsComponent} from '../achats/achats.component';
import {AjustementsComponent} from '../ajustements/ajustements.component';
import {InfosComponent} from '../infos/infos.component';
import {PasswordComponent} from '../password/password.component';
import {InscriptionComponent} from '../inscription/inscription.component';
//import {HomeComponent} from './home/home.component';

const routes: Routes = [
  {
    path: 'mon-profil',
    component: ProfileComponent
  },
  {
    path: 'mes-achats',
    component: AchatsComponent
  },
  {
    path: 'mes-dettes',
    component: AjustementsComponent
  },
  {
    path: 'infos',
    component: InfosComponent
  },
  {
    path: 'erreur',
    component: ErreurComponent
  },
  {
    path: 'inscription_nouvelle_edition',
    component: InscriptionComponent
  },
  {
    path: 'mot_de_passe_oublie',
    component: PasswordComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes,{
    useHash: false,
    relativeLinkResolution: 'legacy'
})
  ],
  exports: [
    RouterModule
  ],
})
export class HomeRoutingModule { }
