import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import {HomeRoutingModule} from './home-routing.module';
import {RouterModule, Routes} from '@angular/router';
import {ProfileComponent} from '../profile/profile.component';


@NgModule({
  imports: [HomeRoutingModule]
})
export class HomeModule { }
