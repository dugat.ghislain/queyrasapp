import { Component, OnInit } from '@angular/core';
import {ApiService} from '../services/api.service';
import {Router} from '@angular/router';
import {FormControl, FormGroup} from '@angular/forms';
import {UserService} from '../services/user.service';

@Component({
  selector: 'app-achats',
  templateUrl: './achats.component.html',
  styleUrls: ['./achats.component.css']
})
export class AchatsComponent implements OnInit {
  user = JSON.parse(localStorage.getItem("user"))
  clicked = false
  achats = []
  achatForm: FormGroup = new FormGroup({
    libelle: new FormControl(),
    montant: new FormControl(),
    alcool: new FormControl(),
  });
  montant = 0;
  alcool = 0;
  achats_total = 0.00
  constructor(private apiService: ApiService, private userService: UserService) { }

  ngOnInit(): void {
    this.userService.refreshUser((user) => {
      this.user = user
      this.refreshAchats()
    })
  }

  supprimerAchat(event, id) {
    if(this.clicked) return
    this.clicked = true
    if(confirm("T'es sûr de vouloir supprimer cet achat ?")) {
      this.apiService.deleteAchat(id).subscribe((res) => {
        if(res != "ok"){
          alert("erreur")
        }
        else{
          this.refreshAchats()
        }
        this.clicked = false
      })
    } else{
      this.clicked = false
    }
  }

  submitAjoutAchat() {
    if(this.clicked) return
    this.clicked = true
    var libelle = this.titleCaseWord(this.achatForm.get('libelle').value)
    if(libelle == null || libelle == "" || this.montant == null || this.montant == 0){
      alert('Il y a des erreurs (libelle ou montant vide)')
      this.clicked = false
      return;
    }
    var alcool = this.achatForm.get('alcool').value
    if(alcool == "true"){
      this.apiService.insertAchatAlcool(this.user.id, libelle, this.montant).subscribe((res) => {
        if(res != "ok"){
          alert(res)
        }
        else{
          this.refreshAchats()
        }
        this.clicked = false
      })
    } else{
      this.apiService.insertAchat(libelle, this.montant).subscribe((res) => {
        if(res != "ok"){
          this.clicked = false
          alert(res)
        }
        else{
          this.refreshAchats()
          this.clicked = false
        }
      })
    }

  }

  titleCaseWord(word: string) {
    if (!word) return word;
    return word[0].toUpperCase() + word.substr(1).toLowerCase();
  }

  refreshAchats(){
    this.apiService.getAchats().subscribe((res) => {
      this.achatForm.get('libelle').setValue("")
      this.achatForm.get('montant').setValue(0)
      this.achatForm.get('alcool').setValue("false")
      this.achats = res
      this.achats_total = 0.00
      for (const achat of this.achats) {
        this.achats_total += parseFloat(achat.montant)
      }
    })
  }
}
