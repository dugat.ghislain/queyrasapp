import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';


import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LayoutModule } from '@angular/cdk/layout';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatTabsModule} from '@angular/material/tabs';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import {PopupLieu, ProfileComponent} from './profile/profile.component';
import { NavigationComponent } from './navigation/navigation.component';
import {MatDividerModule} from '@angular/material/divider';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import {MatDialogModule} from '@angular/material/dialog';
import { ErreurComponent } from './erreur/erreur.component';
import { AchatsComponent } from './achats/achats.component';
import { AjustementsComponent } from './ajustements/ajustements.component';
import {NgxDocViewerModule} from 'ngx-doc-viewer';
import {GoogleSheetsDbService} from 'ng-google-sheets-db';
import {InfosComponent, Telephone} from './infos/infos.component';
import { PasswordComponent } from './password/password.component';
import { InscriptionComponent } from './inscription/inscription.component';


@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
    AppRoutingModule,
    LayoutModule,
    MatFormFieldModule,
    MatButtonModule,
    MatIconModule,
    MatSelectModule,
    MatTabsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatDividerModule,
    MatCheckboxModule,
    MatRadioModule,
    MatDialogModule,
    NgxDocViewerModule
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    ProfileComponent,
    NavigationComponent,
    PopupLieu,
    ErreurComponent,
    AchatsComponent,
    AjustementsComponent,
    InfosComponent,
    Telephone,
    PasswordComponent,
    InscriptionComponent
  ],
  providers: [GoogleSheetsDbService],
  bootstrap: [AppComponent]
})
export class AppModule { }
