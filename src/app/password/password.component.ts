import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {FormControl, FormGroup} from '@angular/forms';
import {ApiService} from '../services/api.service';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css']
})
export class PasswordComponent implements OnInit {

  passwordForm: FormGroup = new FormGroup({
    email: new FormControl(),
  });

  constructor(private apiService: ApiService, private route:Router) { }

  ngOnInit(): void {
    if(localStorage.getItem("editionId") != null && localStorage.getItem("editionId") != "null"){
      this.route.navigate(['/mon-profil']);
    }
  }

  submitPasswordForgotten() {
    this.apiService.submitPasswordForgotten(this.passwordForm.get("email").value).subscribe((res)=>{
      if(res == null || res.error){
        alert('Mauvais login/mot de passe.')
        return
      }
      alert("Un nouveau mot de passe a été attribué à ton compte. Vérifie tes mails.")
      this.route.navigate(['/'])
    })
  }

}
