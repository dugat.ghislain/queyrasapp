// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'http://localhost:8081',
  lienExcel: 'https://docs.google.com/spreadsheets/d/1MXOQOzT5DKe0CcP4iWoAPQtfyvb2gYjaU6bK0uK795g/edit?usp=sharing',
  nb_jours_une_semaine: 8.00,
  nb_jours_sejour: 14.00,
  nb_jours_a_arrondir: 5.00,
  jour_semaine_1_2: "30/07/2022",
  lienCharte: 'https://docs.google.com/document/d/1neZ57iIakT9hH6jg3EcUkZsEKGNFG4tO3lSgphPeh8Q/edit?usp=sharing',
  coeff_voiture: 0.25
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
